/**
 * 工程师自定义遮盖物
 * @type {BMap.Overlay}
 */
function PersonOverlay(point, text, mouseoverText) {
    this._point = point;
    this._text = text;
    this._overText = mouseoverText;
}
// 构建地图图表html
function buildHtml(){

}
PersonOverlay.prototype = new BMap.Overlay();
PersonOverlay.prototype.initialize = function (map) {
    this._map = map;
    var div = this._div = document.createElement("div");
    div.className = 'eng-box'

    var img1 = document.createElement("img");
    img1.className = 'left-icon';
    img1.src = 'http://47.92.89.147/defalut/map/eng/eng_person.jpg'
    div.appendChild(img1);

    var img2 = document.createElement("img");
    img2.className = 'bottom-icon';
    img2.src = 'http://47.92.89.147/defalut/map/eng/eng_bottom.jpg'
    div.appendChild(img2);

    var contentDiv = document.createElement("div");
    contentDiv.className = 'eng-text-box';

    var contentSpan = document.createElement("span");
    contentSpan.className = 'eng-content-text';
    contentDiv.appendChild(contentSpan)

    div.appendChild(contentDiv);
    contentSpan.appendChild(document.createTextNode(this._text));

    map.getPanes().labelPane.appendChild(div);
    return div;
}
PersonOverlay.prototype.draw = function () {
    var map = this._map;
    var pixel = map.pointToOverlayPixel(this._point);
    this._div.style.left = pixel.x -52 + "px";
    this._div.style.top = pixel.y -52+ "px";
}


/**
 * 设备自定义遮盖物
 * @type {BMap.Overlay}
 */
function DeviceOverlay(point, text, mouseoverText) {
    this._point = point;
    this._text = text;
    this._overText = mouseoverText;
}

DeviceOverlay.prototype = new BMap.Overlay();
DeviceOverlay.prototype.initialize = function (map) {
    this._map = map;
    var div = this._div = document.createElement("div");
    div.className = 'eng-box'

    var img1 = document.createElement("img");
    img1.className = 'left-icon-atm';
    img1.src = 'http://47.92.89.147/defalut/map/eng/atm_left.jpg'
    div.appendChild(img1);

    var img2 = document.createElement("img");
    img2.className = 'bottom-icon';
    img2.src = 'http://47.92.89.147/defalut/map/eng/atm_bottom.jpg'
    div.appendChild(img2);

    var contentDiv = document.createElement("div");
    contentDiv.className = 'eng-text-box';

    var contentSpan = document.createElement("span");
    contentSpan.className = 'eng-content-text';
    contentDiv.appendChild(contentSpan)

    div.appendChild(contentDiv);
    contentSpan.appendChild(document.createTextNode(this._text));

    map.getPanes().labelPane.appendChild(div);
    return div;
}
DeviceOverlay.prototype.draw = function () {
    var map = this._map;
    var pixel = map.pointToOverlayPixel(this._point);
    this._div.style.left = pixel.x -52 + "px";
    this._div.style.top = pixel.y -52+ "px";
}
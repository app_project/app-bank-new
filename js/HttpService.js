var $Service = {
    // 短信服务
    sms: {
        // 获取短信验证码
        getCode: function (params, cb, errorCb) {
            Resource.get(SMS_GET_CODE, params, function (data) {
                cb(data);
            }, errorCb)
        },
        // 校验用户验证码
        checkSMSCode: function (params, cb, errorCb) {
            Resource.post(SMS_CHECK_CODE, params, cb, {}, errorCb)
        }
    },
    user: {
        // 更新用户密码
        updateAccountPWD(params, cb, errorCb) {
            Resource.post(USER_SET_NEW_PWD_API, params, cb, {}, errorCb)
        }
    }
}

// HOST = 'http://192.168.99.172:7002';
HOST = 'http://jhapi.yixiuhuo.com';
// HOST = 'http://47.92.89.147:7002';
LOCAL_APP_VERSION = '1.1.6';

// 图片上传
UPLOAD_API = HOST + '/api/upload';
// 校验app版本
CHECK_VERSION_API =  HOST +'/api/version/getPdVersion'
// 校验app版本
DOWN_APP_API =  'http://47.92.89.147/defalut/app-pd/prd/'


//登录
LOGIN_API = HOST + '/api/pd/login';
//退出
USER_LOG_OUT_API = HOST+'/api/pd/logout';


//设备查询接口
DEVICE_API = HOST + '/api/pd/device/getByCode';
// 内部保修查询
DEVICE_INLINE_API = HOST + '/api/pd/device/getByCodeOfInside';

//订单相关
//添加
ORDER_ADD_API = HOST + '/api/pd/order/add';
// 内部保修
ORDER_ADD_INLINE_API = HOST + '/api/pd/order/addInsideOrder';
// 内部转维护商
ORDER_DISPATCHER_TO_TMP_API = HOST + '/api/pd/order/doTurnTpm';
//订单详情
ORDER_DETAIL_API = HOST + '/api/pd/order/getAcceptOrderDetailsById';
//订单维系详情
ORDER_DETAIL_FIX_API = HOST + '/api/pd/order/queryRepairDetailsByOrderId';
//取消订单
ORDER_CANCEL_API = HOST + '/api/pd/order/doCancelOrder';
// 内部报修--取消订单
ORDER_SELF_FIX_API = HOST+'/api/pd/order/doOwnRepair';
// 自行检查
ORDER_INNER_CHECK_API = HOST+ '/api/pd/order/startChecking';

//订单列表--待接单
ORDER_LIST_DAIJIE_API = HOST + '/api/pd/order/getConductOrderList';
//订单列表--历史订单
ORDER_LIST_HISTORY_API = HOST + '/api/pd/order/getHistoryOrderList';
//订单查看
ORDER_DETAIL_COMMON_API = HOST + '/api/pd/order/getAcceptOrderDetailsById';
//订单对接人
ORDER_ADD_CONTACT_API = HOST + '/api/pd/user/getUsersByCurrentUserOrg';
//订单评价
ORDER_ADD_COMMON_API = HOST + '/api/pd/order/evaluateOrder';
//修改预约时间
ORDER_UPDATE_BOOKED_TIME_API = HOST + '/api/pd/order/editDateDate';
//修改对接人
ORDER_CONTACT_PERSON_API = HOST + '/api/pd/order/editLinkmanNameByOrder';



//用户详情
USER_INFO_API = HOST + '/api/pd/user/get';
//更新用户信息
USER_UPDATE_API = HOST + '/api/pd/user/update';
//更新用户信息
USER_CHANGE_WORK_API = HOST + '/api/pd/user/get';
//通讯录
USER_CONTACT_API  = HOST + '/api/pd/user/getUsersByCurrentUserOrg';
//通讯录-new
USER_CONTACT_NEW_API  = HOST + '/api/pd/user/getInsSideUsrOfGroup';
// 网点--- 查询当前登录人子机构
USER_ORG_LIST_API = HOST + '/api/pd/user/listByCurrentOrgAndChildrenOrg';
// 网点--- 添加人员
USER_NET_ADD_USER_API = HOST + '/api/pd/user/addTempPerson';
// 网点--- 删除人员
USER_NET_DEL_USER_API = HOST + '/api/pd/user/del';
// 网点--- 信息人员
USER_NET_INFO_USER_API = HOST + '/api/pd/user/getUserInfoById';
// 用户更新密码
USER_UPDATE_PASSWORD_API = HOST + '/api/pd/user/updateNewPwd';
//消息
MESSAGE_LIST_API = HOST + '/api/business/sms/getByReceiveUserId';
// 设置新密码
USER_SET_NEW_PWD_API = HOST + '/api/business/sms/doPWDNew';




// 工作量统计
STATISTICS_ORDER_WORK_TOTAL = HOST + '/api/dataReport/order/orderDataReportByWorkloadOfTotal';
// 所有机具类型
STATISTICS_DEVICE_TYPE = HOST + '/api/dataReport/common/getLinkByAtmType';
// 所有服务商
STATISTICS_SERVICER = HOST + '/api/dataReport/common/getLinkByMaintainerTenant';
// 机具品牌统计
STATISTICS_DEVICE_BRAND = HOST + '/api/dataReport/device/deviceDataReportCountBySupplier';
// 机具服务商统计
STATISTICS_DEVICE_SERVER = HOST + '/api/dataReport/device/deviceDataReportCountByTpm';
// 订单用时统计
STATISTICS_ORDER_TIME = HOST + '/api/dataReport/order/orderDataReportAvgByTpm';
// 订单类型统计
STATISTICS_ORDER_TYPE = HOST + '/api/dataReport/order/orderDataReportByRevisionClass';

// 订单用时统计
STATISTICS_ORDER_ERROR_TOTAL = HOST + '/api/dataReport/order/orderDataReportByFaultModule';
// 订单类型统计
STATISTICS_ORDER_ERROR_DAY = HOST + '/api/dataReport/order/orderDataReportByFaultModuleOfDate';
// 订单重复保修统计
STATISTICS_ORDER_REPEAT_DAY = HOST + '/api/dataReport/order/queryOrdersReportRepeatRepairs';




// 地图调度
// 获取工程师的位置
DISPATCHER_ENG_LIST = HOST + '/api/business/map/order/engineerGoToWorkList';
// 获取地图所有坐标点
DISPATCHER_POINT_LIST = HOST + '/api/business/map/order/getMapDispEngOAtm';
// 查询机具维修历史
DISPATCHER_DEVICE_FIX_HISTORY_LIST = HOST + '/api/business/map/order/atmRepairRecord';
// 任务列表
DISPATCHER_TASK_LIST = HOST + '/api/business/map/order/platformList';
// 订单修改时间
DISPATCHER_MODIFY_TIME_HISTORY_LIST = HOST + '/api/business/map/order/queryOrderModifyDateDateLogsByOrderId';
// 人员列表
DISPATCHER_PERSION_LIST = HOST + '/api/business/map/order/engineerGoToWorkList';

// 清分
// 查询机具
QING_FEN_GET_ORDER_BILL = HOST + '/api/pd/device/getByCodeOfClearBill';
// 添加清分订单
QING_FEN_Add_ORDER_BILL = HOST + '/api/pd/clearBill/addBill';
// 添加清分订单LIST
QING_FEN_LIST_ORDER_BILL = HOST + '/api/pd/clearBill/queryJdClearBillsPageLimit';
// 删除清分订单
QING_FEN_DEL_ORDER_BILL = HOST + '/api/pd/clearBill/delClearBillById';
// 保存清分金额
QING_FEN_SAVE_MONENY_ORDER_BILL = HOST + '/api/pd/clearBill/saveBillMoneyById';
// 核对清分金额
QING_FEN_AUDIT_MONENY_ORDER_BILL = HOST + '/api/pd/clearBill/auditOperateById';

// 获取短信验证码
SMS_GET_CODE = HOST + '/api/business/sms/getAuthCode';
// 校验验证码
SMS_CHECK_CODE = HOST + '/api/business/sms/checkAuthCode';

var mainPages = ['pages/message/index.html', 'pages/order/list.html', 'pages/user/index.html', 'HBuilder']
var ORDER_STATUS_MAP = {
    '0': '待接单',
    '1': '已接单',
    '2': '已出发',
    '3': '已到达',
    '4': '维修中',
    '5': '已完成',
    '6': '未完成',
    '7': '已取消',
    '9': '未完成',
    '11': '待确认',
    '12': '自行修复',
    '13': '等待处理',
    '14': '开始检查',
    '15': '转维护商',
}
// 用户权限
USER_PERMISSION = 'USER_PERMISSION';
PERSON_SEX_MAP = {
    '0': '男',
    '1': '女'
}
//fastclick 配置
if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', function () {
        FastClick.attach(document.body);
    }, false);
}

/**
 * tab 底部导航监听
 */
mui.plusReady(function () {
    plus.navigator.setStatusBarStyle('dark');
    self = plus.webview.currentWebview();
    self.addEventListener('show', function () {
        var current = plus.webview.currentWebview().id;
        if (['pages/message/index.html', 'pages/order/list.html', 'pages/user/index.html'].indexOf(current) >= 0) {
            var data = {};
            var main = plus.webview.getLaunchWebview();
            switch (current) {
                case 'pages/message/index.html':
                    data = {
                        title: '消息',
                        tab: 'pages/message/index.html'
                    }
                    break;
                case 'pages/order/list.html':
                    data = {
                        title: '订单',
                        tab: 'pages/orderList.html'
                    }
                    break;
                case 'pages/user/index.html':
                    data = {
                        title: '个人中心',
                        tab: 'pages/uCenter.html'
                    }
                    break;
            }
            // plus.navigator.setStatusBarStyle('light');
            console.log('current === ' + current);
            //mui.fire(main, 'changeTab', data);
            //mui.fire(plus.webview.currentWebview(), 'changeTab', data);

        } else {
            // plus.navigator.setStatusBarStyle('dark');
        }

    });
    initNavTap();
})

/**
 * 重定向
 * @param pageFullUrl
 * @param style
 */
function redirect(pageFullUrl, _style, _animate) {
    var style = {
        top: '0px'
    };
    var animate = 'slide-in-right';
    redirectByCommon(pageFullUrl, style, animate);
}

function redirectByCommon(pageFullUrl, style, animate) {
    pageUrl = pageFullUrl.split("?")[0];
    var params = parseQueryString(pageFullUrl);
    console.log('连接link = ' + pageFullUrl + '  参数: ' + JSON.stringify(params));
    var page = plus.webview.getWebviewById(pageUrl);
    if (!page) {
        console.log('open new page ' + pageUrl);
        mui.openWindow({
            url: "/" + pageUrl,
            id: pageUrl,
            styles: style,
            extras: params,
            createNew: false,
        });
        // plus.webview.open("/" + pageUrl, pageUrl, style, animate, 300);
    } else {
        if (['pages/message/index.html', 'pages/order/list.html', 'pages/user/index.html'].indexOf(page.id) >= 0) {
            var mainPage = plus.webview.getWebviewById('HBuilder');
            plus.webview.show(mainPage, 'fade-in', 300);
        }
        console.log('page ' + pageUrl + ' is loaded');
        mui.fire(page, 'changeTab', {
            'params': params
        });
        // page.show(animate, 300);
        plus.webview.show(page, animate, 300);
    }
}

function redirectByTop(pageFullUrl) {
    var style = {
        top: '0px'
    };
    var animate = 'slide-in-bottom';
    plus.webview.open("/" + pageFullUrl, pageFullUrl, style, animate, 300);
}


function redirectByFadeIn(pageFullUrl) {
    var mainPage = plus.webview.getWebviewById('HBuilder');
    mui.fire(mainPage, 'selectedIndex', {
        'params': {}
    });
    var w = document.documentElement.clientWidth, fz = w / 36;
    var style = {
        top: fz * 7 + 'px',
    };
    var animate = 'fade-in';
    plus.webview.show(mainPage, 'fade-in', 300);
    // plus.webview.open("/" + pageFullUrl, pageFullUrl, style, animate, 300);
    redirectByCommon(pageFullUrl, style, animate);
}

/**
 * 关闭当前页面，跳转主页面
 * @param pageFullUrl
 */
function redirectMainPage(pageFullUrl) {
    var ws = plus.webview.currentWebview();
    plus.webview.close(ws);
    var orderAddNav = plus.webview.getWebviewById('pages/order/add/orderAddNav.html');
    plus.webview.close(orderAddNav);
    var style = {
        top: '0px'
    };
    var animate = 'fade-in';
    var mainPage = plus.webview.getWebviewById('HBuilder');
    plus.webview.show(mainPage, 'fade-in', 300);
    redirectByCommon(pageFullUrl, style, animate);

}

function toLogoutPage() {
    // 关闭所有的除去主页面的 webview
    var all = plus.webview.all();
    for (var i = 0, len = all.length; i < len; i++) {
        if (mainPages.indexOf(all[i].id) < 0) {
            all[i].close();
        }
    }
    var style = {
        top: '0px'
    };
    var animate = 'fade-in';
    plus.webview.open("/pages/login/login.html", 'pages/login/login.html', style, animate, 300);
}

/**
 * 返回后，刷新指定页面
 * @param id
 */
function changePage(id) {
    var page = plus.webview.getWebviewById(id);
    mui.fire(page, 'changeTab', {
        'params': {}
    });
    plus.webview.show(id, 'fade-in');
}

/**
 * 刷新指定页面
 * @param id
 */
function refreshPage(id) {
    var ws = plus.webview.currentWebview();
    plus.webview.close(ws, 'none', 0);
    var orderAddNav = plus.webview.getWebviewById('pages/order/searchList.html');
    plus.webview.close(orderAddNav);
    var page = plus.webview.getWebviewById(id);
    mui.fire(page, 'changeTab', {
        'params': {}
    });
    var main = plus.webview.getLaunchWebview();
    // main.show();
    plus.webview.show(page, 'fade-in', 300);
}
/*关闭指定页面通过ID*/
function  closeWebViewById(id) {
    var view = plus.webview.getWebviewById(id);
    plus.webview.close(view);
}
/**
 * 跳转地图
 * @param xCode
 * @param yCode
 * @param mapType
 */
function goMapPage(xCode, yCode, mapType) {
    redirect('pages/common/map.html?' + 'xCode=' + xCode + '&yCode=' + yCode + '&mapType=' + mapType)
}

function parseQueryString(url) {
    var reg_url = /^[^\?]+\?([\w\W]+)$/,
        reg_para = /([^&=]+)=([\w\W]*?)(&|$|#)/g,
        arr_url = reg_url.exec(url),
        ret = {};
    if (arr_url && arr_url[1]) {
        var str_para = arr_url[1],
            result;
        while ((result = reg_para.exec(str_para)) != null) {
            ret[result[1]] = result[2];
        }
    }
    return ret;
}

/**
 * 监听导航事件
 */
function initNavTap() {
    mui("body").on('tap', '.header-nav .icon', function (e) {
        mui.back();
        return true;
    })
}

/**
 * 去登陆
 */
function goLogin() {
    // redirect('pages/login/login.html')
    // plus.webview.open('/pages/login/login.html', 'pages/login/login.html', {top:0}, 'fade-in', 300);

    //窗口参数
    var fullStyles = {
        top: '0px'
    };

    var sub = plus.webview.create("/pages/login/login.html", "pages/login/login.html", fullStyles);
    sub.show();

}


/*------------------------service服务-------------------------------*/
Resource = {
    "post": function (url, data, callback, header, errorBack) {
        var token = getUserToken();
        //console.log("url=" + url, 'token' + token, "body=" + JSON.stringify(data));
        var request = {
            url: url,
            type: 'post',
            data: data,
            headers: {
                'access-token': token
            },
            dataType: 'json',
            success: function (res) {
                plus.nativeUI.closeWaiting();
                console.log('current page = ' + self.id + ' response=' + JSON.stringify(res));
                if (res.code == "000000") {
                    if (callback) {
                        callback(res['data']);
                    }
                } else if (res.code == "000005" || res.code == "000003") {
                    //跳转登录页面
                    goLogin();
                } else {
                    //提示错误信息
                    mui.toast(res.msg);
                    if (errorBack) {
                        errorBack();
                    }
                }
            },
            error: function (e) {
                plus.nativeUI.closeWaiting();
                console.log("post提交异常：" + JSON.stringify(e));
                mui.toast('网络错误');
            }
        }
        mui.ajax(request);
    },
    "get": function (url, params, callback, errorBack) {
        var token = getUserToken();
        console.log('url = ' + url + ' current page = ' + self.id + ' data =' + JSON.stringify(params) + 'tocken: ' + token);

        mui.ajax({
            url: url,
            type: 'GET',
            headers: {
                'access-token': token
            },
            data: params,
            dataType: 'json',
            success: function (res) {
                // plus.nativeUI.closeWaiting();
                console.log('current page = ' + self.id + ' data=' + JSON.stringify(res));

                if (res.code == '000000') {
                    if (callback) {
                        callback(res.data);
                    } else {
                        console.log('无回调函数。。。。。。')
                    }
                } else if (res.code == "000005" || res.code == "000003") {
                    //跳转登录页面
                    goLogin();
                } else {
                    mui.toast(res.msg);
                    if (errorBack) {
                        errorBack();
                    }
                }
            },
            error: function (e) {
                plus.nativeUI.closeWaiting();
                console.log(JSON.stringify(e))
                mui.toast('网络错误');
            },
            complete: function () {
            }
        })
    }
}


/*********---------web 本地session存储相关------********************--start***/
var storage = localStorage;
//登录key
var LOGIN_TOKEN = "LOGIN_TOKEN";
//图片服务地址
var IMG_SERVER = 'IMG_SERVER';
//用户名密码
var USER_FORM = 'USER_FORM';
// 用户信息
var USER_INFO = 'USER_INFO'
// 重置用户密码id
var RESET_PWD_USER_ID = 'RESET_PWD_USER_ID';

/*****用户相关 *******/
//用户真实姓名
var USER_REALNAME = 'USER_REALNAME';

/**
 * 获得本地存储
 * @param key
 * @returns {null}
 */
function getItem(key) {
    try {
        return JSON.parse(storage.getItem(key))
    } catch (err) {
        console.log("从本地存储中获取数据错误" + err)
        return null
    }
};

/**
 * 存放数据
 * @param key
 * @param val
 */
function setItem(key, val) {
    if (!key) {
        throw new Error('存储key不能为空');
    }
    storage.setItem(key, JSON.stringify(val))
};

/**
 * 清理所有存储key
 */
function clear() {
    storage.clear()
};

/**
 * 获得所有key
 */
function keys() {
    return storage.keys()
};

/**
 * 移除key
 * @param key
 */
function removeItem(key) {
    storage.removeItem(key)
}

/*********---------web 本地session存储相关------********************--end ***/


/*********--------- 用户服务相关------********************--start ***/
/**
 * 存储图片服务地址
 * @param url
 */
function setImgServer(url) {
    setItem(IMG_SERVER, url);
};

/**
 * 获取图片服务器key
 * @returns {*}
 */
function getImgServer() {
    return getItem(IMG_SERVER);
};

/**
 * 设置用户名
 * @param realname
 */
function setUserName(realname) {
    setItem(USER_REALNAME, realname);
};

/**
 * 获得用户名
 * @param realname
 */
function getUserName() {
    return getItem(USER_REALNAME);
};


/*********--------- 登录退出相关------********************--start ***/
/**
 * 设置token
 * @param token
 */
function setUserToken(token) {
    setItem(LOGIN_TOKEN, token);
};

/**
 * 获得token
 */
function getUserToken() {
    return getItem(LOGIN_TOKEN);
};

/**
 * 退出登录 清除所有数据
 */
function delUserToken() {
    clear();
};


/**
 *获取存储的用户名密码
 */
function getRememberUser() {
    return getItem(USER_FORM);
}

/**
 *设置存储的用户名密码
 * @param formUser
 */
function setRememberUser(formUser) {
    setItem(USER_FORM, formUser);
}

/**
 * 设置用户权限
 */
function setUserPermission(data) {
    var userData = {
        "token": data.token,
        "statistics": data['app-pd-tjbb'] === 'OPERATION_APPLY', // 统计
        "imgServer": data['imgServer'],
        "mapDispatch": data['app-pd-map'] === 'OPERATION_APPLY', // 地图调度
        "innerOrderDispatcher": data['app-pd-nbbx'] === 'OPERATION_APPLY', // 内部报修
        "qingFen": data['app-pd-qf'] === 'OPERATION_APPLY', // 清分
        "useManual": data['useManual'], // 是否显示用户手册
    }
    setItem(USER_PERMISSION, userData);
}

/**
 * 获取用户权限
 */
function getUserPermission() {
    console.log(JSON.stringify(getItem(USER_PERMISSION)));
    return getItem(USER_PERMISSION);
}

/*********--------- 登录退出相关------********************--end ***/


function setUserInfo(userInfo) {
    setItem(USER_INFO, userInfo)
}

function getUserInfo() {
    getItem(USER_INFO)
}

/*********--------- 用户服务相关------********************--start ***/

/**
 * 解析时间戳
 * @param timeStamp
 * @returns {*}
 */
function parseTimeStamp(timeStamp) {
    var date = new Date(timeStamp * 1);
    Y = date.getFullYear() + '-';
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes() + ':';
    s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + h + m + s;
}

/**
 * 解析时间戳 无小时秒
 * @param timeStamp
 * @returns {*}
 */
function parseTimeStampNoHours(timeStamp) {
    var date = new Date(timeStamp * 1);
    Y = date.getFullYear() + '-';
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes() + ':';
    s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + '00:00:00';
}

/**
 * 解析时间戳 无小时秒
 * @param timeStamp
 * @returns {*}
 */
function parseTimeStampNoHoursStart(timeStamp) {
    var date = new Date(timeStamp * 1);
    Y = date.getFullYear() + '-';
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
    h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
    m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes() + ':';
    s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
    return Y + M + D + '23:59:59';
}

function getTimeField(timeStamp, type) {
    var date = new Date(timeStamp * 1);
    Y = date.getFullYear()
    M = date.getMonth() + 1;
    D = date.getDate();
    h = date.getHours();
    m = date.getMinutes();
    switch (type) {
        case 1:
            return Y;
        case 2:
            return M;
        case 3:
            return D;
        case 4:
            return h;
        case 5:
            return m;
        default:
            return Y;
    }

}

/**
 * 拨打电话
 * @param phone
 */
function goCallPhone(phone) {
    if (!phone) {
        return
    }
    location.href = 'tel:' + phone;
}


/**
 * 地图 展示用户头像
 * @param img
 * @returns {*}
 */
function showUserImg(img) {
    if (img) {
        if (img == 'http://47.92.89.147' || img == 'http://47.92.89.147null') {
            return '../../../img/common/img_default_avatar@3x.png'
        }
        return getImgServer() + img;
    } else {
        return '../../../img/common/img_default_avatar@3x.png'
    }
}

/**
 * 订单状态转换
 *  0---空闲 -- 绿
 *  1--- 忙碌 --红
 *  2--- 途中 --蓝
 * @param options
 * @param status
 * @returns {string}
 */
function switchOrderStatus(status) {

    if (['0', '11'].indexOf(status) >= 0) { // 红色
        return '1';
    }
    if (['1', '2', '3'].indexOf(status) >= 0) { // 蓝色
        return '2';
    }
    if (status == '4') { // 绿色
        return '0';
    }
    return '0';

}


//计算版本号大小,转化大小
function toNum(a) {
    var a = a.toString();
    var c = a.split('.');
    var num_place = ["", "0", "00", "000", "0000"], r = num_place.reverse();
    for (var i = 0; i < c.length; i++) {
        var len = c[i].length;
        c[i] = r[len] + c[i];
    }
    var res = c.join('');
    return res;
}


//检测插件版本号是否需要更新
function needUpdate(b) {
    var a = toNum(LOCAL_APP_VERSION);
    var b = toNum(b);
    if (a == b) {
        console.log("版本号相同！版本号为:" + a);
        return false;
    } else if (a > b) {
        console.log("版本号" + a + "是新版本!");
        return false;
    } else {
        console.log("版本号" + b + "是新版本!");
        return true;
    }
}


/**
 * 检查是否需要更新
 */
function checkVersionNew() {
    function noticeUpdate() {
        plus.nativeUI.alert("修乐巴新增了一些功能，您需要升级才能正常使用。", function () {
            plus.runtime.openURL('https://itunes.apple.com/us/app/%E4%BF%AE%E4%B9%90%E5%B7%B4%E6%8A%A5%E4%BF%AE%E7%AB%AF/id1397300308?l=zh&ls=1&mt=8');
            noticeUpdate();
        }, "温馨提示", "去升级");
    }

    Resource.get(CHECK_VERSION_API, {}, function (res) {
        var server_version = res.version;
        if (needUpdate(server_version)) { // 需要更新
            noticeUpdate();
        }

    }, function (error) {
        plus.nativeUI.close();
    })
}

/**
 * 校验输入的清分金额是否符合要求
 * @param value 金额数字
 * @returns {boolean}
 */
function checkInputMoneyIsOk(value) {
    if(value === "0" || value === 0){
        return true;
    }
    var numberMath =  /^(([^0][0-9]+|0)\.([0-9]{1,2})$)|^([^0][0-9]+|0)$/;
    return numberMath.test(value) && (value >= 0.01 && value < 10000000000);
}

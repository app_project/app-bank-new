var mapZIndex = 10000;
/**
 * 工程师自定义遮盖物
 * @type {BMap.Overlay}
 */
function CommonMapOverlay(point, text, options, data, cb) {
    this._point = point;
    this._text = text;
    this._cb = cb;
    this._data = data;
    this._options = options;


}

CommonMapOverlay.prototype = new BMap.Overlay();
CommonMapOverlay.prototype.initialize = function (map) {

    /**
     * 设置圈的颜色
     * @type {string}
     */
    var bgColor = this._options.bgColor;

    var imgUrl = this._options.imgUrl;

    this._map = map;
    var div = this._div = document.createElement("div");
    div.className = 'common-map-layout'
    /*
    * 外侧
    * */
    var divTop = document.createElement("div");
    divTop.className = 'cml-top';

    var divT1 = document.createElement("div");
    divT1.className = 'cml-top-content';
    // 设置外侧颜色 -------颜色设置
    divT1.style.cssText = 'background-color:' + bgColor;

    var img1 = document.createElement("img");
    img1.className = 'cml-icon';
    img1.src = 'http://47.92.89.147/defalut/map/dispatcher/ic_worker@3x.png';
    if (imgUrl) {
        img1.src = imgUrl
    }


    var divT2 = document.createElement("div");
    divT2.className = 'bottom-triangle bt-pos';
    // 三角颜色  ---------颜色设置
    divT2.style.cssText = 'background-color:' + bgColor + '; box-shadow: 10px 9px 10px 0px ' + bgColor + ';border-top: 10px solid ' + bgColor + ';';

    divT1.appendChild(img1);
    divT1.appendChild(document.createTextNode(this._text));

    divTop.appendChild(divT1);
    divTop.appendChild(divT2);

    /**
     * 下部点
     * @type {HTMLElement}
     */
    var divBottom = document.createElement("div");
    divBottom.className = 'cml-bottom-box';

    var divB1 = document.createElement("div");
    divB1.className = 'cml-bottom-outer';

    var divB2 = document.createElement("div");
    divB2.className = 'cml-bottom-inner';
    // 内侧圈颜色
    divB1.style.cssText = 'background-color:' + bgColor;
    // 外侧圈颜色
    divB2.style.cssText = 'background-color:' + bgColor;
    divBottom.appendChild(divB1);
    divBottom.appendChild(divB2);

    /**
     * 最外层包裹
     */
    div.appendChild(divTop)
    div.appendChild(divBottom);
    var that = this;
    div.onclick = function () {
        // 显示到最顶端
        div.style.zIndex = ++mapZIndex;
        if (that._cb) {
            that._cb(that._data, divT1, divT2);
        }
    }
    div.ontouchstart = function () {
        // 显示到最顶端
        div.style.zIndex = ++mapZIndex;
        if (that._cb) {
            that._cb(that._data, divT1, divT2);
        }
    }
    this._divT1 = divT1;
    this._divT2 = divT2;
    map.getPanes().labelPane.appendChild(div);
    return div;
}
CommonMapOverlay.prototype.draw = function () {
    var map = this._map;
    var pixel = map.pointToOverlayPixel(this._point);
    this._div.style.left = pixel.x - 52 + "px";
    this._div.style.top = pixel.y - 45 + "px";
}

/**
 * 人员图表
 * @param xCode
 * @param yCode
 * @param text
 * @param status
 * @param item
 * @param cb
 * @returns {BMap.Overlay}
 */
function buildPerson(xCode, yCode, text, status, item, cb) {
    var options = {
        imgUrl: 'http://47.92.89.147/defalut/map/dispatcher/ic_worker@3x.png'
    };
    getStatusColor(options, status);
    return new CommonMapOverlay(new BMap.Point(xCode, yCode), text, options, item, cb);
}

/**
 * 构建设备信息
 * @param xCode
 * @param yCode
 * @param text
 * @param status
 * @param item
 * @param cb
 * @returns {BMap.Overlay}
 */
function buildDevice(xCode, yCode, text, status, item, cb) {
    var options = {
        imgUrl: 'http://47.92.89.147/defalut/map/dispatcher/img_machines@3x.png'
    };
    getStatusColor(options, status);
    return new CommonMapOverlay(new BMap.Point(xCode, yCode), text, options, item, cb);

}

/**
 * 获取人员状态
 *  0---空闲 -- 绿
 *  1--- 忙碌 --红
 *  2--- 途中 --蓝
 * @param options
 * @param status
 */
function getStatusColor(options, status) {
    switch (status) {
        // 空闲
        case '0':
            options.bgColor = '#58D048';
            options.lBgColor = '#B8E2B2';
            break;
        // 忙碌
        case '1':
            options.bgColor = '#EF6060';
            options.lBgColor = '#EEA6A7';
            break;
        // 途中
        case '2':
            options.bgColor = '#4B93E7';
            options.lBgColor = '#A0C3EB';
            break;
        default:
            options.bgColor = '#4B93E7';
            options.lBgColor = '#A0C3EB';
    }
}
var zIndex = 10000;
// 改变背景颜色为选中状态
function changeBackGroundColor(divT1, divT2) {
    divT1.style.zIndex = ++zIndex;
    var oldBg = divT1.style.backgroundColor;
    var bgColor = '#FFA148';
    divT1.style.cssText = 'background-color:' + bgColor + '';
    divT2.style.cssText = 'background-color:' + bgColor + '; box-shadow: 10px 9px 10px 0px ' + bgColor + ';border-top: 10px solid ' + bgColor + ';';
    return oldBg;
}

// 还原选中颜色
function resetBgColor(divT1, divT2, bgColor) {
    divT1.style.cssText = 'background-color:' + bgColor + '';
    divT2.style.cssText = 'background-color:' + bgColor + '; box-shadow: 10px 9px 10px 0px ' + bgColor + ';border-top: 10px solid ' + bgColor + ';';
}

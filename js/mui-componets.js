/**
 * 事件弹框组件
 * @param pageId
 * @param cb
 * @param textTitle
 */
function selectTime(pageId, cb, textTitle) {
    (function ($) {
        $.init();
        var result = $('#' + pageId+"_result")[0];
        var btns = $('#' + pageId);
        btns.each(function (i, btn) {
            btn.addEventListener('tap', function () {
                var _self = this;
                if (_self.picker) {
                    _self.picker.show(function (rs) {
                        result.innerText = textTitle + rs.text;
                        _self.picker.dispose();
                        _self.picker = null;
                    });
                } else {
                    var optionsJson = this.getAttribute('data-options') || '{}';
                    var options = JSON.parse(optionsJson);
                    var id = this.getAttribute('id');
                    var startDate = new Date(Date.now() + 30 * 60 * 1000).getTime();
                    var _beginYear = getTimeField(startDate, 1);
                    var _beginMonth = getTimeField(startDate, 2);
                    var _beginDay = getTimeField(startDate, 3);
                    var _beginHours = getTimeField(startDate, 4);
                    var _beginMinutes = getTimeField(startDate, 5);
                    console.log(_beginYear, _beginMonth, _beginDay, _beginHours, _beginMinutes);
                    var dataStr = parseTimeStamp(startDate);
                    console.log(dataStr);
                    options = {
                        "type": "minutes",
                        "value": dataStr,
                        "beginYear": _beginYear,
                        "beginMonth": _beginMonth,
                        "beginDay": _beginDay,
                        'beginHours': _beginHours,
                        'beginMinutes': _beginMinutes,
                    }
                    _self.picker = new $.DtPicker(options);
                    _self.picker.show(function (rs) {
                        var time1 = new Date(rs.y.value, (rs.m.value - 1), rs.d.value, rs.h.value, rs.i.value, 0);
                        cb(time1.getTime());
                        result.innerText = rs.y.value + "-" + rs.m.value + "-" + rs.d.value + " " + rs.h.value + ":" + rs.i.value + ":" + "00"
                        _self.picker.dispose();
                        _self.picker = null;
                    });
                }

            }, false);
        });
    })(mui);
}

/**
 * 上传图片
 * @param cb
 */
function uploadPhoto(cb) {
    // 弹出系统选择按钮框
    plus.nativeUI.actionSheet({
        title: "请选择",
        cancel: "取消",
        buttons: [{title: "拍照"}, {title: "相册"}]
    }, function (e) {
        var index = e.index;
        switch (index) {
            case 2://相册选择
                plus.gallery.pick(function (path) {
                    createUpload(path, function (data) {
                        cb(data.data + '');
                        //_self.errorImgShow.push(getImgServer() + data.data)
                    });
                }, function (error) {

                }, {filter: "image"});
                break;
            case 1://拍照
                var cmr = plus.camera.getCamera();
                var res = cmr.supportedImageResolutions[0];
                var fmt = cmr.supportedImageFormats[0];
                cmr.captureImage(function (path) {
                        createUpload(path, function (data) {
                            cb(data.data + '');
                        });
                    },
                    function (error) {
                        //取消
                    },
                    {resolution: res, format: fmt}
                );
                break;
            default:
                ;
        }
    });
}

/**
 * 图片上传
 * @param filePath 文件地址
 * @param funcSuccess 上传成功回调函数
 */
function createUpload(filePath, funcSuccess) {
    const api = UPLOAD_API;
    console.log("host:" + api)
    const random = Math.random("10000") + '0';
    const randStr = random.substring(2, 8);
    const dst = "_doc/bank" + randStr + filePath.split(".")[0] + ".jpg";
    const token = this.getUserToken();
    plus.nativeUI.showWaiting("压缩中");
    plus.zip.compressImage({
            src: filePath,
            dst: dst,
            quality: 20,
            format: 'jpg'
        },
        function () {
            plus.nativeUI.closeWaiting();
            plus.nativeUI.showWaiting("上传中");
            const task = plus.uploader.createUpload(api, {
                    method: "POST",
                    blocksize: 204800,
                    priority: 100
                },
                function (t, status) {
                    plus.nativeUI.closeWaiting()
                    // 上传完成
                    if (status == 200) {
                        const res = JSON.parse(t.responseText)
                        if (res.code == '000000') {
                            funcSuccess(res)
                        } else {
                            mui.toast('上传失败' + res.msg);
                        }
                    } else {
                        mui.toast(网络异常);
                    }

                }
            );
            //添加上传文件
            task.addFile(dst, {
                key: "file"
            });
            // 设置自定义数据头
            task.setRequestHeader('access-token', token);
            task.start();
        },
        function (error) {
            plus.nativeUI.closeWaiting();
        });

}

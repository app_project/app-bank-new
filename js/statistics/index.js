// 工作量统计
function buildWorkData(data, res) {
    var titleData = res.title;
    var totalData = [];
    var pmData = [];
    var fixData = [];
    if (res.data.length > 0) {
        totalData = res.data[2].list;
        pmData = res.data[1].list;
        fixData = res.data[0].list;
    }
    data.total = {
        title: '订单总量',
        lineColor: '#FFA148',
        xData: titleData,
        yData: totalData
    };
    data.pm = {
        title: 'PM量',
        lineColor: '#7DD272',
        xData: titleData,
        yData: pmData
    };
    data.fix = {
        title: '维修量',
        lineColor: '#76B4EE',
        xData: titleData,
        yData: fixData
    };
}

// 机具品牌统计
function buildDevicekData(data, res, type) {
    if (type === 1) {
        data.brand = {
            title: '机具品牌(个)',
            xData: res.title,
            yData: res.data
        }
    } else {
        data.server = {
            title: '机具维护商(个)',
            lineColor: '#7DD272',
            xData: res.title,
            yData: res.data
        }
    }
}

// 订单数据
function buildOrderData(data, res, type) {
    if (type === 1) {
        data.time = {
            title: '订单用时分析(分钟)',
            xData: res.title,
            yData: res.data
        }
    } else {
        data.type = {
            title: '订单类型分析',
            lineColor: '#7DD272',
            xData: res.title,
            yData: res.data
        }
    }
}

// 订单数据
function buildOrderWithTimeData(data, res) {
    var yData = [];
    var oldData = res.data;
    for (var i = 0; i < oldData.length; i++) {
        yData.push(parseInt(oldData[i] / 60));
    }
    data.time = {
        title: '订单用时分析(分钟)',
        xData: res.title,
        yData: yData
    }

}

// 获取参数
function getTimeOrOtherParams(timeIndex, rightObj) {
    var result = {}
    switch (timeIndex) {
        case 0:
            result.lastWeek = 'lastWeek';
            break;
        case 1:
            result.thisWeek = 'thisWeek';
            break;
        case 2:
            result.lastMonth = 'lastMonth';
            break;
        case 3:
            result.thisMonth = 'thisMonth';
            break;
    }
    getRightData(rightObj, result);
    return result;
}

/**
 * 获取维护商 或 机具类型参数
 * @param rightObj
 * @param result
 */
function getRightData(rightObj, result) {
    if (rightObj.type === 1 && rightObj.value) { // 机具类型
        result.type = rightObj.value;
    } else if (rightObj.type === 2 && rightObj.value) { // 维护商
        result.tenantDisId = rightObj.value;
    }

}

/**
 * 获取维护商 或 机具类型参数
 * @param rightObj
 * @param result
 */
function getRightDataORderRepeat(rightObj, result) {
    if (rightObj.type === 1 && rightObj.value) { // 机具类型
        result.atmType = rightObj.value;
    } else if (rightObj.type === 2 && rightObj.value) { // 维护商
        result.tenantDisId = rightObj.value;
    }

}

function buildFailureData(data, res) {
    data.total = {
        title: '故障模块汇总(个)',
        xData: res.title,
        yData: res.data,
        type: 'big'
    }
}

/**
 * 获取订单重复维修参数
 */
function getRepairOrderParams(repeatDay,daySelectIndex, rightObj) {
    var result = {};
    var now = Date.now();
    result.endDate = parseTimeStampNoHoursStart(now);
    result.repeatDays = repeatDay;
    var fixDay = 30;
    switch (daySelectIndex) {
        case 0:
            fixDay = 30;
            break;
        case 1:
            fixDay = 60;
            break;
        case 2:
            fixDay = 90;
            break;

    }
    result.beginDate = getDayTimeStr(now, -fixDay);

    getRightDataORderRepeat(rightObj, result);
    return result;
}

/**
 * 获得 now 时间后的天，时间格式
 * @param now
 * @param step
 * @returns {*}
 */
function getDayTimeStr(now, step) {
    return parseTimeStampNoHours(now + step * 1 * 24 * 60 * 60 * 1000)
}
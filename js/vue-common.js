/**
 * vue 多选组件
 */
Vue.component('ls-multi-select', {
    template: '<div class="xlb-multi-btn-box"><div v-for="(val,key) in typeMap" :key="key" @click="select(key)"\n' +
        ':class="{\'select\':types.indexOf(key) >= 0}" class="multi-btn-item">{{val}}</div> </div>',

    props: {
        /**
         * 类型map
         */
        typeMap: {
            type: Object,
            default: function () {
                return {}
            },
            required: true
        },

    },
    data: function () {
        return {
            /**
             * 选择的类型
             */
            types: [],
        }

    },

    methods: {
        //选择故障模块
        select: function (key) {
            if (!key) {
                return null;
            }
            var index = this.types.indexOf(key);
            if (index >= 0) {
                this.types.splice(index, 1);
            } else {
                this.types.push(key);
            }
            this.$emit('input', this.types);
        },
    }
})

/**
 * vue 单选组件
 */
Vue.component('ls-single-select', {
    template: '<div class="xlb-multi-btn-box"><div v-for="(val,key) in typeMap" :key="key" @click="select(key)"\n' +
        ':class="{\'select\':selected ==key}" class="multi-btn-item">{{val}}</div> </div>',

    props: {
        /**
         * 类型map
         */
        typeMap: {
            type: Object,
            default: function () {
                return {}
            },
            required: true
        },

    },
    data: function () {
        return {
            /**
             * 选择的类型
             */
            selected: '',
        }

    },

    methods: {
        //选择故障模块
        select: function (key) {
            this.selected = key;
            this.$emit('input', key);
        },
    }
})


/**
 * vue 评论组件
 */
Vue.component('xlb-comment', {
    template: '<div>' +
                    ' <div class="comment-box">' +
                    '     <img @click="dealSelect(index,item)" v-for="(item, index) in startArray"  :src="showImg(item)"></div> ' +
                    '<div v-if="!nameLabel" class="comment-text">{{showText}}</div>' +
                '</div>',

    props: {
        /**
         * 类型map
         */
        value: {
            type: Number,String,
            default: function () {
                return 0
            },
            required: true
        },
        nameLabel: {
            type:Boolean,
            default:function () {
                return false;
            },
            required: false
        }

    },

    methods: {
        //选择故障模块
        dealSelect: function (index,item) {
            if(this.nameLabel){
                return
            }
            if(item == 0 || index < this.value){
                this.$emit('input', index+1);
            }else {
                this.$emit('input', index);
            }

        },
        showImg:function (value) {
            if(value ==1 ){
                return '../../img/common/start_full.jpg'
            }
            return '../../img/common/start_empty.jpg'
        }
    },
    computed: {
        startArray: function () {
            var val =parseInt(this.value);
            switch (val) {
                case 1:
                    return [1, 0, 0, 0, 0];
                case 2:
                    return [1, 1, 0, 0, 0];
                case 3:
                    return [1, 1, 1, 0, 0];
                case 4:
                    return [1, 1, 1, 1, 0];
                case 5:
                    return [1, 1, 1, 1, 1];
                default:
                    return [0,0,0,0,0]
            }
        },
        showText:function () {
            if(this.value == 0){
                return ''
            }
            if(this.value < 3){
                return '差评'
            }

            if(this.value == 3){
                return '中评'
            }

            if(this.value > 3){
                return '好评'
            }

        },

    }
})

/**
 * vue 弹框组件
 */
Vue.component('xlb-modal', {
    template: ' <div class="xlb-modal-box" v-show="delMod">\n' +
        '            <div class="shade-mod"></div>\n' +
        '            <div class="shade-context-box">\n' +
        '                <div class="header">\n' +
        '                   <slot name="header"></slot>' +
        '                </div>\n' +
        '                <div class="context">\n' +
        '                   <slot name="content"></slot>' +
        '                </div>\n' +
        '                <div class="footer">\n' +
        '                    <div @click="cancel" class="left"><div class="mid-line"><slot name="footer-left"></slot></div></div>\n' +
        '                    <div @click="confirm" class="right"><div class="mid-line"><slot name="footer-right"></slot></div></div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>',

    props: {
        /**
         * 类型map
         */
        value: {
            type: Boolean,
            default: function () {
                return false;
            },
            required: true
        }
    },
    computed:{
        delMod: function () {
           return this.value;
        }
    },
    methods: {
        cancel:function () {
            this.$emit('input',false)
        },
        confirm:function () {
            this.$emit('on-click',{});
        }
    }
})

/**
 * vue 弹框只有一个按钮组件
 */
Vue.component('xlb-modal-single', {
    template: ' <div v-show="delMod">\n' +
        '            <div class="shade-mod"></div>\n' +
        '            <div class="shade-context-box">\n' +
        '                <div class="header">\n' +
        '                   <slot name="header"></slot>' +
        '                </div>\n' +
        '                <div class="context">\n' +
        '                   <slot name="content"></slot>' +
        '                </div>\n' +
        '                <div class="footer">\n' +
        '                   <slot name="footer"></slot>' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>',

    props: {
        /**
         * 类型map
         */
        value: {
            type: Boolean,
            default: function () {
                return false;
            },
            required: true
        }
    },
    computed:{
        delMod: function () {
            return this.value;
        }
    },
    methods: {
        cancel:function () {
            this.$emit('input',false)
        },
        confirm:function () {
            this.$emit('on-click',{});
        }
    }
})


//时间格式化
Vue.filter('orderStatusFilter', function (value) {
    if (!value) return ''
    return ORDER_STATUS_MAP[value];
})
//时间格式化
Vue.filter('dataFormat', function (value) {
    if (!value) return ''
    return dateFormate(value);
})

//时间格式化
Vue.filter('dataFormatShort', function (value) {
    if (!value) return ''
    return dateFormatShort(value);
})
//时间格式化
Vue.filter('dataFormatYear', function (value) {
    if (!value) return ''
    return dateFormateYear(value);
})

/**
 * 时间戳格式化
 * @param inputTime
 * @returns {*}
 */
function dateFormate(inputTime) {
    if (!inputTime) {
        return '';
    }
    var date = new Date(inputTime);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
}

/**
 * 时间戳格式化
 * @param inputTime
 * @returns {*}
 */
function dateFormateYear(inputTime) {
    if (!inputTime) {
        return '';
    }
    var date = new Date(inputTime);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d;
}

/**
 * 时间戳格式化
 * @param inputTime
 * @returns {*}
 */
function dateFormatShort(inputTime) {
    if (!inputTime) {
        return '';
    }
    var date = new Date(inputTime);
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return  m + '-' + d + ' ' + h + ':' + minute;
}


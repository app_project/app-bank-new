function createChart(el) {
    return echarts.init(el);
}

/**
 * 生成普通折线图
 * @param xData
 * @param showData
 * @param options
 * @returns {{tooltip: {trigger: string, axisPointer: {type: string, label: {backgroundColor: string}}, formatter: (function(*): string)}, grid: {left: string, right: string, bottom: string, containLabel: boolean}, xAxis: {name: string, type: string, data: *, axisLabel: {interval: number}}, yAxis: {name: string, type: string}, series: {name: string, type: string, sampling: string, itemStyle: {color: string}, data: *}[], dataZoom: *[]}}
 */
function initCommonLineConfig(xData, showData, options) {
    var lineColor = '#FFA148';
    if (options && options.lineColor) {
        lineColor = options.lineColor;
    }
    // 指定图表的配置项和数据
    return option = {
        title: {
            left: 'left',
            text: options.title,
        },
        tooltip: {
            // 显示辅助线 x y 轴
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            },
            // formatter: function (params) {
            //     var obj = params[0];
            //     return '时间:' + obj.axisValue + '<br>' + obj.seriesName + ':' + obj.axisValue;
            // }
        },
        grid: {
            left: '5%',
            right: '10%',
            bottom: '5%',
            containLabel: true
        },
        xAxis: {
            name: '时间',
            type: 'category',
            data: xData,
        },
        yAxis: {
            name: '数量',
            type: 'value',
        },
        series: [{
            name: '数量',
            type: 'line',
            symbol: 'none',
            sampling: 'average',
            itemStyle: {
                color: lineColor
            },
            data: showData

        }],
        dataZoom: [
            {
                type: 'inside',
            }, {
                type: 'slider'
            }
        ],

    };
}


/**
 * 获取多条折线
 * @param xData
 * @param yData
 * @returns {{xData: Array, yData: Array}}
 */
function initMultiLineConfig(titleData, yData, options) {

    /**
     * 构建数据
     * @param data
     * @returns {{legend: Array, dataList: Array}}
     */
    function buildData(data) {
        var legend = [];
        var dataList = [];
        for (var i = 0; i < data.length; i++) {
            var name = data[i].name;
            legend.push(name);
            dataList.push({
                name: name,
                type: 'line',
                areaStyle: {normal: {}},
                data: data[i].list
            })
        }
        return {
            legend: legend,
            dataList: dataList
        };
    }

    var seriesData = buildData(yData);

    return option = {
        title: {
            left: 'left',
            text: options.title,
        },
        legend: {
            type: 'scroll',
            orient: 'horizontal',
            x: 'left',
            y: '10%',
            data: seriesData.legend,
        },
        tooltip: {
            // 显示辅助线 x y 轴
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            },
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [{
            name: '时间',
            type: 'category',
            boundaryGap: false,
            data: titleData
        }],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: seriesData.dataList,
        dataZoom: [
            {
                type: 'inside',
            }, {
                type: 'slider'
            }
        ]
    };


}

/**
 * 生成柱状图
 * @param xData
 * @param showData
 * @param options
 * @returns {{title: {left: string, text: *}, tooltip: {trigger: string, axisPointer: {type: string}, formatter: (function(*): string)}, grid: {left: string, right: string, bottom: string, containLabel: boolean}, xAxis: {name: string, type: string, data: *, axisLabel: {interval: number}}, yAxis: {name: string, type: string}, series: {name: string, type: string, symbol: string, sampling: string, itemStyle: {color: string}, data: *}[], dataZoom: {type: string}[]}}
 */
function initCommonBarConfig(xData, showData, options) {
    var lineColor = '#FFA148';
    if (options && options.lineColor) {
        lineColor = options.lineColor;
    }
    // 指定图表的配置项和数据
    return option = {
        title: {
            left: 'left',
            text: options.title,
        },
        tooltip: {
            // 显示辅助线 x y 轴
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            },
            // formatter: function (params) {
            //     var obj = params[0];
            //     return '时间:' + obj.axisValue + '<br>' + obj.seriesName + ':' + obj.axisValue;
            // }
        },
        grid: {
            left: '5%',
            right: '10%',
            bottom: '5%',
            containLabel: true
        },
        xAxis: {
            // name: '时间',
            type: 'category',
            data: xData,
        },
        yAxis: {
            // name: '数量',
            type: 'value',
        },
        series: [{
            name: '数量',
            type: 'bar',
            symbol: 'none',
            sampling: 'average',
            itemStyle: {
                color: lineColor
            },
            data: showData

        }],

    };
}

/**
 * 时间柱状图
 */
function initOrderTimeBarConfig(xData, showData, options) {
    var lineColor = '#FFA148';
    if (options && options.lineColor) {
        lineColor = options.lineColor;
    }
    // 指定图表的配置项和数据
    return option = {
        title: {
            left: 'left',
            text: options.title,
        },
        tooltip: {
            // 显示辅助线 x y 轴
            trigger: 'axis',
            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
            },
            formatter: function (obj) {
                return "平均用时："+ minutesFormat24(obj[0].data)
            }
        },
        grid: {
            left: '5%',
            right: '10%',
            bottom: '5%',
            containLabel: true
        },
        xAxis: {
            // name: '时间',
            type: 'category',
            data: xData,
        },
        yAxis: {
            // name: '数量',
            type: 'value',
            show:false
        },
        series: [{
            name: '时间',
            type: 'bar',
            symbol: 'none',
            sampling: 'average',
            itemStyle: {
                color: lineColor
            },
            data: showData

        }],

    };
}

/**
 * 生成饼状图
 * @param xData
 * @param showData
 * @param options
 * @returns {{title: {left: string, text: *}, tooltip: {trigger: string, axisPointer: {type: string}, formatter: (function(*): string)}, grid: {left: string, right: string, bottom: string, containLabel: boolean}, xAxis: {name: string, type: string, data: *, axisLabel: {interval: number}}, yAxis: {name: string, type: string}, series: {name: string, type: string, symbol: string, sampling: string, itemStyle: {color: string}, data: *}[], dataZoom: {type: string}[]}}
 */
function initCommonPieConfig(xData, showData, options) {
    var lineColor = '#FFA148';
    if (options && options.lineColor) {
        lineColor = options.lineColor;
    }
    var seriesData = [];
    for (var i = 0; i < xData.length; i++) {
        seriesData.push({
            name: xData[i],
            value: showData[i]
        })
    }
    // 指定图表的配置项和数据
    return option = {
        title: {
            left: 'left',
            text: options.title,
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        grid: {
            left: '10%',
            top: '20%',
            bottom: '0',
            containLabel: true
        },
        legend: {
            type: 'scroll',
            orient: 'horizontal',
            x: 'left',
            y: '10%',
            data: xData,
        },
        series: [
            {
                name: options.title,
                type: 'pie',
                radius: ['30%', '60%'],
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '20',
                            fontWeight: 'bold'
                        }
                    }
                },
                lableLine: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: true
                    }
                },
                data: seriesData
            }
        ]
    };
}
/**
 * 生成饼状图
 * @param xData
 * @param showData
 * @param options
 * @returns {{title: {left: string, text: *}, tooltip: {trigger: string, axisPointer: {type: string}, formatter: (function(*): string)}, grid: {left: string, right: string, bottom: string, containLabel: boolean}, xAxis: {name: string, type: string, data: *, axisLabel: {interval: number}}, yAxis: {name: string, type: string}, series: {name: string, type: string, symbol: string, sampling: string, itemStyle: {color: string}, data: *}[], dataZoom: {type: string}[]}}
 */
function initBigPieConfig(xData, showData, options) {
    var lineColor = '#FFA148';
    if (options && options.lineColor) {
        lineColor = options.lineColor;
    }
    var seriesData = [];
    for (var i = 0; i < xData.length; i++) {
        seriesData.push({
            name: xData[i],
            value: showData[i]
        })
    }
    // 指定图表的配置项和数据
    return option = {
        title: {
            left: 'center',
            text: options.title,
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        grid: {
            left: '10%',
            top: '20%',
            bottom: '0',
            containLabel: true
        },
        legend: {
            type: 'scroll',
            orient: 'horizontal',
            left: 'center',
            bottom: 10,
            data: xData
        },
        series: [
            {
                name: options.title,
                avoidLabelOverlap: false,
                type: 'pie',
                radius: ['40%', '80%'],
                label: {
                    normal: {
                        show: false,
                        position: 'center'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '20',
                            fontWeight: 'bold'
                        }
                    }
                },
                lableLine: {
                    normal: {
                        show: false
                    },
                },
                data: seriesData
            }
        ]
    };
}

// 渲染
function drawChart(chart, option) {
    // 使用刚指定的配置项和数据显示图表。
    setTimeout(function () {
        chart.setOption(option,true);
        chart.resize();
    },200)

}

/**
 * 分钟格式化为小时格式
 * @param minutes
 * @returns {string}
 */
function minutesFormat24(minutes) {
    minutes = parseInt(minutes)
    var day = parseInt(minutes/(60*24));
    var hour = parseInt(minutes / 60 % 24);
    var min = parseInt(minutes % 60);
    var result = "";
    // console.log(minutes+" "+day+ " "+ hour + " " +min)
    // if (day > 0) {
    //     result = day + "天";
    // }
    if (hour > 0) {
        result += day*24+ hour + "小时";
    }else {
        result += "0小时";
    }
    if (min > 0) {
        result += parseFloat(min) + "分";
    }else {
        result += "0分";
    }

    return result;
}

/**
 * vue 按钮组
 */
Vue.component('btn-group-common', {
    template: ' <div class="btn-group header-btn">' +
        '            <div v-for=" (item, index) of options" @click="dealSelect(item,index)" class="btn-item" :class="{ \'btn-item-actived\' : value == index }">{{item}}</div>' +
        '        </div>',

    props: {
        /**
         * 类型map
         */
        options: {
            type: Array,
            default: function () {
                return [];
            },
            required: true
        },
        value: {
            type: [String, Number],
            default: function () {
                return '';
            }
        }
    },
    computed: {
        delMod: function () {
            return this.value;
        }
    },
    methods: {
        dealSelect: function (item, index) {
            this.$emit("input", index);
        }
    }
})

/**
 * header 多选项卡切换
 */
Vue.component('btn-header-select', {
    template: '<span>' +
        '      <span @click="showList"> {{optionData[value]}} <img class="header-title-img" :src="imgSrc"></span>' +
        '                <div v-show="shadeMod" @click="closeMod">' +
        '                   <div class="xlb-shade"></div> ' +
        '                   <div class="btn-select-box">' +
        '                      <div class="btn-header">{{optionData[value]}}<img class="header-title-img" :src="imgSrc"></div>' +
        '                      <div @click="dealSelect(index)" v-for="(item,index) of optionData" :class="{active:index == value }">{{item}}</div>' +
        '                    </div>' +
        '                </div>' +
        '     </span>',

    props: {
        optionData: {
            type: Array,
            default: function () {
                return [];
            }
        },
        imgSrc: {
            type: String,
            default: function () {
                return '/img/common/btn_unfold@3x.png';
            }
        },
        value: {
            type: Number,
            default: function () {
                return 0;
            }
        }
    },
    data: function () {
        return {
            shadeMod: false,

        }
    },
    methods: {
        showList: function () {
            this.shadeMod = !this.shadeMod;
            forbiddenBodyScroll(this.shadeMod);
        },
        dealSelect: function (index) {
            this.$emit('input', index);
        },
        closeMod: function (index) {
            this.showList();
        },


    }
})
/**
 *  header 筛选
 */
Vue.component('btn-header-sort', {
    template: '<div class="header-right"><img @click="showList" :src="imgSrc">' +
        '        <div  v-show="shadeMod" class="ls-sort-box">' +
        '                <div @click="closeMod" class="xlb-shade"></div>' +
        '                <div class="ls-sort-list-box">' +
        '                    <div class="ls-sort-list-header">{{titleName}}</div>' +
        '                    <div class="ls-sort-list-content" >' +
        '                    <div @click="dealSelectAll" >全部</div>' + // :class="{actived:!value }"
        '                        <div @click="dealSelect(item.id)" :class="{actived:value == item.id }" v-for=" item of optionData">{{item.name}}<img v-show="itemSelected.indexOf(item.id) >= 0" class="xlb-modal-checked" src="../../../img/common/ic_selected@2x.png" alt=""></div>' +
        '                    </div>' +
        '                    <div @click="doSelected" v-show="multi" class="footer-btn" >确定</div>' +
        '                </div>' +
        '            </div>' +
        '        </div>',

    props: {
        optionData: {
            type: Array,
            default: function () {
                return [];
            }
        },
        multi: false,
        titleName: '',
        imgSrc: {
            type: String,
            default: function () {
                return '/img/common/btn_screen@3x.png';
            }
        },
        value: {
            type: [Number, String],
            default: function () {
                return '';
            }
        }
    },
    watch: {
        value: function (val) {
            console.log(" value change " + val);
            this.itemSelected = val.split(',');
        }
    },
    data: function () {
        return {
            shadeMod: false,
            itemSelected: [],

        }
    },
    methods: {
        showList: function () {
            this.shadeMod = !this.shadeMod;
            forbiddenBodyScroll(this.shadeMod);
        },
        dealSelect: function (key) {
            if (this.multi) { // 判断是否是多选
                var index = this.itemSelected.indexOf(key);
                if (index >= 0) {
                    this.itemSelected.splice(index, 1);
                } else {
                    this.itemSelected.push(key);
                }
            } else {
                this.closeMod();
                this.itemSelected = [key]
                this.$emit('input', key);
            }
        },
        // 确定
        doSelected: function () {
            this.$emit('input', this.itemSelected + '');
            this.closeMod();
        },
        closeMod: function (index) {
            this.showList();
        },
        showSelected: function (id) {
            if (this.multi) {
                var value = this.value.split(',');
                return value.indexOf(id) >= 0;
            }
            return this.value === id;
        },
        dealSelectAll: function () { // 选择所有
            this.itemSelected = [];
            this.$emit('input', '');
            this.closeMod();
        }

    }
})

/**
 * 禁止body页面滚动或恢复滚动
 */
function forbiddenBodyScroll(flag) {
    if (flag) {
        document.body.style.overflow = 'hidden'
    } else {
        document.body.style.overflow = 'auto'
    }

}


Vue.component('user-change-repeat-day', {
    template: '<xlb-modal v-model="show">\n' +
        '            <div class="dialog-content order-fix-modal" slot="content">\n' +
        '                设备按照 <input class="fix-input" v-model="inputFixDay" type="tel"> 个工作日计算重复维修\n' +
        '            </div>\n' +
        '            <div slot="footer-left" @click="cancel">取消</div>\n' +
        '            <div slot="footer-right" @click="save">提交</div>\n' +
        '        </xlb-modal>',
    props: {
        day: {
            default: function () {
                return 30;
            }
        },
        show: {
            default() {
                return false;
            }
        }
    },
    data: function () {
        return {
            inputFixDay: 30
        }
    },
    watch: {
        show: function (val) {
            if (val) {
                this.inputFixDay = this.day;
            }
        }
    },
    methods: {
        cancel: function () {
            this.$emit('update:show', false)
        },
        save: function () {
            if (this.inputFixDay <= 0) {
                return mui.toast('重复天数必须大于 0');
            }
            this.$emit("change", this.inputFixDay);
            this.cancel();
        }
    }

})

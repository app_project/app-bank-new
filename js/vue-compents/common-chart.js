// 折线图表
Vue.component('common-line-chart', {
    template: '<div ref="chartDom" class="common-chart"></div>',
    props: {
        options: {
            type: Object,
            default: function () {
                return {
                    xData: [],
                    yData: [],
                    lineColor: '#2e4cff',
                    title: ''
                }
            }
        }
    },
    data: function () {
        return {
            chart: null
        }
    },
    mounted: function () {
        this.chart = createChart(this.$refs.chartDom);
        this.initChart();
    },
    methods: {
        initChart: function () {
            this.chart.clear();
            drawChart(this.chart, initCommonLineConfig(this.options.xData, this.options.yData, {
                lineColor: this.options.lineColor,
                title: this.options.title
            }));
        }
    },
    watch: {
        options: function () {
            this.initChart();
        }
    }

})

// 多条折线图表
Vue.component('multi-line-chart', {
    template: '<div ref="chartDom" class="common-chart"></div>',
    props: {
        options: {
            type: Object,
            default: function () {
                return {
                    xData: [],
                    yData: [],
                    lineColor: '#2e4cff',
                    title: ''
                }
            }
        }
    },
    data: function () {
        return {
            chart: null
        }
    },
    mounted: function () {
        this.chart = createChart(this.$refs.chartDom);
        this.initChart();
    },
    methods: {
        initChart: function () {
            this.chart.clear();
            drawChart(this.chart, initMultiLineConfig(this.options.xData, this.options.yData, {
                title: this.options.title
            }));
        }
    },
    watch: {
        options: function () {
            this.initChart();
        }
    }

})

// 柱状图图表
Vue.component('common-bar-chart', {
    template: '<div ref="chartDom" class="common-chart"></div>',
    props: {
        options: {
            type: Object,
            default: function () {
                return {
                    xData: [],
                    yData: [],
                    lineColor: '#2e4cff',
                    title: ''
                }
            }
        }
    },
    data: function () {
        return {
            chart: null
        }
    },
    mounted: function () {
        this.chart = createChart(this.$refs.chartDom);
        this.initChart();
    },
    methods: {
        initChart: function () {
            this.chart.clear();
            drawChart(this.chart, initCommonBarConfig(this.options.xData, this.options.yData, {
                lineColor: this.options.lineColor,
                title: this.options.title
            }));
        }
    },
    watch: {
        options: function () {
            this.initChart();
        }
    }

})

// 柱状图图表
Vue.component('order-time-bar-chart', {
    template: '<div ref="chartDom" class="common-chart"></div>',
    props: {
        options: {
            type: Object,
            default: function () {
                return {
                    xData: [],
                    yData: [],
                    lineColor: '#2e4cff',
                    title: ''
                }
            }
        }
    },
    data: function () {
        return {
            chart: null
        }
    },
    mounted: function () {
        this.chart = createChart(this.$refs.chartDom);
        this.initChart();
    },
    methods: {
        initChart: function () {
            this.chart.clear();
            drawChart(this.chart, initOrderTimeBarConfig(this.options.xData, this.options.yData, {
                lineColor: this.options.lineColor,
                title: this.options.title
            }));
        }
    },
    watch: {
        options: function () {
            this.initChart();
        }
    }

})


// 饼状图
Vue.component('common-pie-chart', {
    template: '<div ref="chartDom" class="common-chart"></div>',
    props: {
        options: {
            type: Object,
            default: function () {
                return {
                    xData: [],
                    yData: [],
                    lineColor: '#2e4cff',
                    title: '',
                    type:'common'
                }
            }
        }
    },
    data: function () {
        return {
            chart: null
        }
    },
    mounted: function () {
        this.chart = createChart(this.$refs.chartDom);
        this.initChart();

    },
    methods: {
        initChart: function () {
            this.chart.clear();
            drawChart(this.chart, this.getInitConfig());
        },
        getInitConfig:function () {
            var config = '';
            switch (this.options.type) {
                case 'big':
                    config = initBigPieConfig(this.options.xData, this.options.yData, {
                        lineColor: this.options.lineColor,
                        title: this.options.title
                    });
                    console.log('big------')
                    break;
                default:
                    config = initCommonPieConfig(this.options.xData, this.options.yData, {
                        lineColor: this.options.lineColor,
                        title: this.options.title
                    });

            }

            return config;
        }
    },
    watch: {
        options: function () {
            this.initChart();
        }
    }

})
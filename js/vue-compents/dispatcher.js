Vue.component('dmap-right-legend', {
    template: '<div v-show="value">' +
        '        <div class="lm-modal-outer shade-gray" @click="closeMod" ></div>\n' +
        '        <div class="lm-legend-box">\n' +
        '            <div class="lm-legend-box-item">\n' +
        '                <div class="lm-lb-title">工程师</div>\n' +
        '                <div class="le-item-box" >\n' +
        '                    <div class="le-ib-icon">\n' +
        '                        <div class="cml-bottom-outer status-space outer-color"></div>\n' +
        '                        <div class="cml-bottom-inner status-space"></div>\n' +
        '                    </div>\n' +
        '                    <span>空闲</span>\n' +
        '                </div>\n' +
        '                <div class="le-item-box" >\n' +
        '                    <div class="le-ib-icon">\n' +
        '                        <div class="cml-bottom-outer status-working outer-color"></div>\n' +
        '                        <div class="cml-bottom-inner status-working"></div>\n' +
        '                    </div>\n' +
        '                    <span>途中</span>\n' +
        '                </div>\n' +
        '                <div class="le-item-box" >\n' +
        '                    <div class="le-ib-icon">\n' +
        '                        <div class="cml-bottom-outer status-busy outer-color"></div>\n' +
        '                        <div class="cml-bottom-inner status-busy"></div>\n' +
        '                    </div>\n' +
        '                    <span>忙碌</span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="lm-legend-box-item">\n' +
        '                <div class="lm-lb-title">机具</div>\n' +
        '                <div class="le-item-box" >\n' +
        '                    <div class="le-ib-icon">\n' +
        '                        <div class="cml-bottom-outer status-space outer-color"></div>\n' +
        '                        <div class="cml-bottom-inner status-space"></div>\n' +
        '                    </div>\n' +
        '                    <span>维修中</span>\n' +
        '                </div>\n' +
        '                <div class="le-item-box" >\n' +
        '                    <div class="le-ib-icon">\n' +
        '                        <div class="cml-bottom-outer status-working outer-color"></div>\n' +
        '                        <div class="cml-bottom-inner status-working"></div>\n' +
        '                    </div>\n' +
        '                    <span>已接单</span>\n' +
        '                </div>\n' +
        '                <div class="le-item-box" >\n' +
        '                    <div class="le-ib-icon">\n' +
        '                        <div class="cml-bottom-outer status-busy outer-color"></div>\n' +
        '                        <div class="cml-bottom-inner status-busy"></div>\n' +
        '                    </div>\n' +
        '                    <span>报修中</span>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '\n' +
        '    </div>',
    props: {
        value: false
    },
    methods: {
        closeMod: function () {
            this.$emit('input', false)
        }
    }

})
// 用户状态card
Vue.component('lm-status-span', {
    template: '<span>' +
        '<span v-if="type == 0" class="lm-status-span dm-de-status status-space">空闲</span>' +
        '<span v-if="type == 1" class="lm-status-span dm-de-status status-busy">忙碌</span>' +
        '<span v-if="type == 2" class="lm-status-span dm-de-status status-working">途中</span>' +
        '</span>',
    props: {
        type: {
            default:function () {
                return 1;
            }
        }
    }
})

// 状态显示card
Vue.component('lm-order-status-span', {
    template: '<span>' +
        '<span v-if="type == 0" class="lm-status-span dm-de-status status-space"><slot></slot></span>' +
        '<span v-if="type == 1" class="lm-status-span dm-de-status status-busy"><slot></slot></span>' +
        '<span v-if="type == 2" class="lm-status-span dm-de-status status-working"><slot></slot></span>' +
        '</span>',
    props: {
        type: {
            default:function () {
                return 1;
            }
        }
    }
})

/**
 * 用户信息card
 */
Vue.component('user-mir-info-card',{
    template:'<div>' +
        '            <div class="dm-eng-info" v-for="item of dataInfo">' +
        '                <div class="dm-eng-box-left">' +
        '                    <div class="dm-eng-img-box"' +
        '                         :style="{backgroundImage: \'url(\' + showUserImg(item.icon) + \')\'}"></div>\n' +
        '                </div>' +
        '                <div class="dm-eng-right">\n' +
        '                    <div><span class="dm-eng-box-title">{{item.engName}}</span> <span v-if="item.engPhone">（{{item.engPhone}}）</span></div>\n' +
        '                    <div>所在单位：{{item.deptName}}</div>\n' +
        '                </div>' +
        '                <div class="dm-eng-call">\n' +
        '                    <img @click="callPhone(item.engPhone)" src="../../../img/common/btn_phone@3x.png" alt="">\n' +
        '                </div>' +
        '            </div>' +
        '    </div>',

    props: {
        dataInfo: {
            default:function () {
                return []
            }
        }
    },

    methods:{
        showUserImg: function (icon) {
           return showUserImg(icon)
        },
        callPhone: function (phone) {
            goCallPhone(phone);
        }
    }

})


Vue.component('user-info-sample',{
    template: '<div>' +
        '              <div class="tx-eng-box tx-border " v-for="item of data" >\n' +
        '                    <div class="tx-eng-img" ' +
        '                         :style="{backgroundImage: \'url(\' + showUserImg(item.icon) + \')\'}"></div>\n' +
        '                    <div class="tx-eng-text">\n' +
        '                       {{item.name}}（{{item.phone}}）' +
        '                    </div>\n' +
        '              </div>' +
        ' </div>',
    props: {
        data: {
            default:function () {
                return []
            }
        }
    },
    methods:{
        showUserImg: function (icon) {
            return showUserImg(icon)
        },
        callPhone: function (phone) {
            goCallPhone(phone);
        }
    }
})

Vue.component('user-line-item',{
    template: '      <div class="card-line-item">' +
        '                <div class="cli-img" ' +
        '                         :style="{backgroundImage: \'url(\' + showUserImg(data.icon) + \')\'}"></div>' +
        '                <div class="cli-right-text">{{data.name}} <lm-status-span :type="data.status"></lm-status-span></div>' +
        '            </div>',
    props: {
        data: {
            default:function () {
                return {
                    name: '',
                    icon: '',
                    phone: '',
                    status: ''
                }
            }
        }
    },
    methods:{
        showUserImg: function (icon) {
            return showUserImg(icon)
        },
        callPhone: function (phone) {
            goCallPhone(phone);
        }
    }

})



Vue.component('login-header', {
    template: '<div>' +
        '    <img class="logo" src="../../img/login/img_appicon.png" alt="">' +
        '    <div class="header">修乐巴用户登录</div>' +
        ' </div>'
})

Vue.component('login-input-content', {
    template: '<div class="input_box">\n' +
        '                <div class="input_outer">\n' +
        '                    <input v-model="userName" type="text" placeholder="请输入用户名">\n' +
        '                    <div class="label_left">\n' +
        '                        <img class="input_icon" src="../../img/login/ic_user.png">\n' +
        '                        <span class="label_text">用户名</span>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="input_outer" style="margin-bottom: 0">\n' +
        '                    <input @keyup.enter="doLogin" v-model="password" :type="inputType"' +
        '                           style="text-transform:none;" placeholder="请输入密码">\n' +
        '                    <div class="label_left">\n' +
        '                        <img class="input_icon" src="../../img/login/ic_password.png">' +
        '                        <span class="label_text">密码</span>' +
        '                    </div>\n' +
        '                    <div class="label_right" @touchstart="showPassword">' +
        '                        <img v-if="passwordMod" class="right_icon"' +
        '                             src="../../img/login/ic_invisible.png">' +
        '                        <img v-else="passwordMod" class="right_icon"' +
        '                             src="../../img/login/btn_visible.png">' +
        '                    </div>\n' +
        '                </div>\n' +
        '    </div>',
    props: ['value'],
    data: function () {
        return {
            password: '',
            userName: '',
            inputType: 'password',
            passwordMod: true,
        }
    },
    methods: {
        // 显示密码
        showPassword: function () {
            this.passwordMod = !this.passwordMod;
            if (this.passwordMod) {
                this.inputType = 'password'
            } else {
                this.inputType = 'text'
            }
        },
        doLogin:function() {
            this.$emit('login', true)
        },
        buildData:function () {
            return {
                password: this.password,
                userName: this.userName,
            }
        }
    },
    watch: {
        password: function () {
            this.$emit('input', this.buildData())
        },
        userName: function () {
            this.$emit('input', this.buildData())
        }
    }
})

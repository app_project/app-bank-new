Vue.component('order-error-history-info',{
    template:'<div class="card-box order-info">\n' +
        '            <div class="order-title">故障信息</div>\n' +
        '            <div>\n' +
        '                <span class="label-weight">报修类型:</span>\n' +
        '                <span class="label-light">{{orderDetail.atmErrorStatusName}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">故障模块:</span>\n' +
        '                <span class="label-light">{{orderDetail.startUnTypeName}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">故障描述:</span>\n' +
        '                <span class="label-light">{{orderDetail.startUnDesc}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">建单人:</span>\n' +
        '                <span class="label-light">{{orderDetail.createUserName}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">建单人电话:</span>\n' +
        '                <span class="label-light">{{orderDetail.createUserPhone}}</span>\n' +
        '            </div>\n' +
        '            <div>\n' +
        '                <span class="label-weight">维修反馈:</span>\n' +
        '                <span class="label-light">{{orderDetail.fixDescribe}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '        </div>',
    props:['orderDetail']
});

Vue.component('order-error-info',{
    template:'<div class="card-box order-info">\n' +
        '            <div class="order-title">故障信息</div>\n' +
        '            <div>\n' +
        '                <span class="label-weight">报修类型:</span>\n' +
        '                <span class="label-light">{{orderDetail.atmErrorStatusName}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">故障模块:</span>\n' +
        '                <span class="label-light">{{orderDetail.startUnTypeName}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">故障描述:</span>\n' +
        '                <span class="label-light">{{orderDetail.startUnDesc}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">建单人:</span>\n' +
        '                <span class="label-light">{{orderDetail.createUserName}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">建单人电话:</span>\n' +
        '                <span class="label-light">{{orderDetail.createUserPhone}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '        </div>',
    props:['orderDetail']
});


Vue.component('order-simple-info',{
    template:'<div class="card-box order-info">\n' +
        '            <div class="order-title">订单编号：{{orderDetail.orderNo}} <span v-show="showStatus" class="order-status-text" >{{orderDetail.orderStatus | orderStatusFilter}}</span></div>\n' +
        '            <div>\n' +
        '                <span class="label-weight">建单时间:</span>\n' +
        '                <span class="label-light">{{ orderDetail.beginDate | dataFormat}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">机具信息:</span>\n' +
        '                <span class="label-light"> {{ orderDetail.atmCode}}-{{orderDetail.modelCode}}-{{orderDetail.atmInsSide}}-{{ orderDetail.serialNumber}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">分行:</span>\n' +
        '                <span class="label-light">{{ orderDetail.orgGradeA}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">一级支行:</span>\n' +
        '                <span class="label-light">{{ orderDetail.orgGradeB}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '        </div>',
    props:['orderDetail','showStatus']
});


Vue.component('order-user-info',{
    template:'<div class="order-user-info">' +
                '<div v-for="(item,index) in orderDetail.orderEngineerVos ">\n' +
                '                <div class="card-box eng-box">\n' +
                '                    <div class="eng-header">\n' +
                '                        <span class="left">网点人员</span> <span \n' +
                '                            class="right">{{orderDetail.orderStatus | orderStatusFilter}}</span>\n' +
                '                    </div>\n' +
                '                    <div class="eng-header-desc">\n' +
                '                        <p><img class="eng-img" :src="showCommonImg(item.engIcon)" alt=""></p>\n' +
                '                        <div class="eng-info">\n' +
                '                            <div class="name">{{item.engName}}</div>\n' +
                '                            <div class="company">所在单位：{{item.engDeptName}}</div>\n' +
                '                        </div>\n' +
                '                        <img @click="callPhone(item.engPhone)" class="icon-call" src="../../../img/common/ic_phone.png"\n' +
                '                             alt="">\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '</div>',
    props:['orderDetail'],
    methods:{
        goUserMap: function (xCode, yCode) {
            goMapPage(xCode, yCode, 1);
        },
        showCommonImg: function (img) {
            if (img) {
                return getImgServer() + img;
            } else {
                return '../../../img/common/default_person.jpg'
            }
        },
        //打电话
        callPhone: function (phone) {
            goCallPhone(phone);
        },
    }
})
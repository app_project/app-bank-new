Vue.component('order-add-card', {
    template: ' ' +
        '    <div v-show="show" class="eng-modal-card emc-shadow" :class="{close: animateClose,enter:!animateClose}">' +
        '       <div style="position: fixed;width: 100%;bottom: 18rem;top: 0;z-index: 99999" @click="closeMod" ></div>' +
        '        <div @click="dealSelect(1,$event)" class="emc-item">' +
        '            <img src="component/img/btn_engineer@3x.png" alt="">' +
        '            <div>找工程师</div>' +
        '        </div>' +
        '        <div @click="dealSelect(2,$event)" class="emc-item">' +
        '            <img src="component/img/btn_fault@3x.png" alt="">' +
        '            <div>故障确认</div>' +
        '        </div>' +
        '    </div>',


    props: {
        show: {
            type: Boolean,
            default: function () {
                return false;
            }
        }
    },
    data: function () {
        return {
            animateClose: false
        }
    },
    methods: {
        closeMod: function(){
           this.actionAnimate(false);
        },
        showItems: function () {
            this.modalShow = !this.modalShow;
        },
        dealSelect: function (index, e) {
            this.actionAnimate(true,index);
        },
        actionAnimate:function (result,value) {
            this.animateClose =true;
            var that = this;
            setTimeout(function () {
                that.$emit('selected-change', result,value);
                that.animateClose = false;
            }, 400)
        }
    }
})
function actionAnimate(obj) {

}

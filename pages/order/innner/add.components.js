// 设备信息
Vue.component('add-order-device-info', {
    template: ' <div class="card-box order-info">\n' +
        '            <div class="order-title">机具信息</div>\n' +
        '            <div>\n' +
        '                <span class="label-weight">机具编号:</span>\n' +
        '                <span class="label-light">{{data.code}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">机具类型:</span>\n' +
        '                <span class="label-light">{{data.typeName}}</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div>\n' +
        '                <span class="label-weight">网点名称:</span>\n' +
        '                <span class="label-light">{{data.insSide}}</span>\n' +
        '            </div>\n' +
        '        </div>',
    props: ['data']
});
// 特殊需求
Vue.component('add-order-special-info', {
    template: ' <div class="card-box li">\n' +
        '            <div class="title">\n' +
        '                <span class="label-light ">特殊需求</span>\n' +
        '            </div>\n' +
        '\n' +
        '            <div class="xlb-li">\n' +
        '                <div class="left">预约时间</div>\n' +
        '                <div id=\'yuyueTime\' class="right">\n' +
        '                    <span id="yuyueTime_result" data-options=\'{}\' style="text-align: center">无预约</span>\n' +
        '                    <img src="../../../img/common/btn_more.png" alt="">\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="xlb-li" @click="goContact">\n' +
        '                <div class="left">联系人</div>\n' +
        '                <div class="right">{{abutmentUserName}} <img src="../../../img/common/btn_more.png" alt=""></div>\n' +
        '            </div>\n' +
        '        </div>',
    props: ['abutmentUserName'],
    methods: {
        goContact: function () {
            redirect('pages/order/contact/index.html')
        }
    }
})

Vue.component('xlb-select-img', {
    template: '<div>' +
                '<div class="card-box device-info">\n' +
                '                <span class="label-light ">故障照片</span>\n' +
                '                <!-- 已经上传的照片 -->\n' +
                '                <div class="img-list">\n' +
                '                    <div class="img-item" v-for="imgUrl in errorImgShow ">\n' +
                '                        <div class="img-close" @click="delImg(imgUrl)" @click="delImg(imgUrl)">X</div>\n' +
                '                        <img :src="imgUrl" @click="showImg(imgUrl)">\n' +
                '                    </div>\n' +
                '                    <div class="img-item">\n' +
                '                        <img @click="selectPhoto" src="../../../img/common/btn_addpic@3x.png" alt="">\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '       </div>' +
                '       <div v-if="imgShow" class="img-show-box">\n' +
                '            <div @click="closeImgShow" :class="{\'img-show-box-shade\':imgShow}"></div>\n' +
                '            <img @click="closeImgShow" :src="imgShowUrl" alt="">\n' +
                '</div>' +
            '</div>',
    data: function () {
        return {
            errorImgShow: [],
            errorImg:[],
            imgShow:false,
            imgShowUrl:false
        }
    },
    methods: {
        //上传照片
        selectPhoto: function () {
            const _self = this;
            uploadPhoto(function (data) {
                _self.errorImg.push('' + data)
                _self.errorImgShow.push(getImgServer() + data);
                _self.changeValue();
            })
        },

        delImg: function (imgUrl) {
            var imgServer = getImgServer();
            var img = imgUrl.replace(imgServer, '');
            this.errorImg.splice(this.errorImg.indexOf(img + ''), 1);
            this.errorImgShow.splice(this.errorImgShow.indexOf(imgUrl), 1);
            this.changeValue();
        },
        showImg: function (img) {
            this.imgShow = true;
            this.imgShowUrl = img
        },
        closeImgShow: function () {
            this.imgShow = false;
        },
        changeValue:function () {
            this.$emit('input',this.errorImg+'');
        }
    }
})